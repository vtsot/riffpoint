-- phpMyAdmin SQL Dump
-- version 3.5.6
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Фев 02 2015 г., 11:39
-- Версия сервера: 5.5.29
-- Версия PHP: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `riffpoint_test_task`
--

-- --------------------------------------------------------

--
-- Структура таблицы `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2D5B0234F92F3E70` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `city`
--

INSERT INTO `city` (`id`, `country_id`, `name`) VALUES
(1, 2, 'Kharkov'),
(2, 2, 'Poltava'),
(3, 2, 'Kiev'),
(4, 3, 'Moscow'),
(5, 3, 'Belgorod'),
(6, 3, 'Kursk'),
(7, 4, 'Warsaw');

-- --------------------------------------------------------

--
-- Структура таблицы `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `country`
--

INSERT INTO `country` (`id`, `name`) VALUES
(1, 'USA'),
(2, 'UA'),
(3, 'RU'),
(4, 'PL');

-- --------------------------------------------------------

--
-- Структура таблицы `fos_admin`
--

CREATE TABLE IF NOT EXISTS `fos_admin` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `fos_admin`
--

INSERT INTO `fos_admin` (`id`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Структура таблицы `fos_man`
--

CREATE TABLE IF NOT EXISTS `fos_man` (
  `id` int(11) NOT NULL,
  `body_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `fos_man`
--

INSERT INTO `fos_man` (`id`, `body_type`) VALUES
(23, NULL),
(24, NULL),
(25, NULL),
(26, NULL),
(27, NULL),
(28, NULL),
(29, NULL),
(30, NULL),
(31, NULL),
(32, NULL),
(33, NULL),
(34, NULL),
(35, NULL),
(36, NULL),
(37, NULL),
(38, NULL),
(39, NULL),
(40, NULL),
(41, NULL),
(42, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `fos_person`
--

CREATE TABLE IF NOT EXISTS `fos_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `sex` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_day` date DEFAULT NULL,
  `marital_status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CEF52DCC92FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_CEF52DCCA0D96FBF` (`email_canonical`),
  KEY `IDX_CEF52DCCF92F3E70` (`country_id`),
  KEY `IDX_CEF52DCC8BAC62AF` (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=43 ;

--
-- Дамп данных таблицы `fos_person`
--

INSERT INTO `fos_person` (`id`, `country_id`, `city_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `sex`, `first_name`, `last_name`, `birth_day`, `marital_status`, `type`) VALUES
(1, NULL, NULL, 'admin@admin.ad', 'admin@admin.ad', 'admin@admin.ad', 'admin@admin.ad', 1, 'bap4zluyktck8ckocwk88gwckg4kkcc', 'NBbKMGVYKBBS8KnNusZnhMMNfUnfA8/P8XMrtOwhZSLLm8NiXBhShVzmV0WzCImCr/0nG48ZbpYbwD1s0a7Ngw==', NULL, 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'user'),
(2, NULL, NULL, 'user@user.us', 'user@user.us', 'user@user.us', 'user@user.us', 1, '2nrfff6bzxkw4cs0g0s88kkc0wkksoc', 'eb8s8BbpEJ677Edz9Gwn6jsEHII1894rMF302W2lHBqSA8dksxhPSNLJ1E3vnIQm1h+zau+hF9osxX4n9NKe7A==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'user'),
(3, 2, 1, 'woman1@admin.ad', 'woman1@admin.ad', 'woman1@admin.ad', 'woman1@admin.ad', 1, 'l15q7xzj5yoswwkoowc8wcgwc8ocscw', 'HMk1PzmQ2lqCRqECE1CvaYZebaBTopejbM+Va7MSJp2HW1idK/6FX3mc+Uji+s29n8ngiJqku6tw2WJUGFlwaA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman1firstname', 'woman1lasttname', '2015-01-31', 'not_marrie', 'woman'),
(4, 2, 1, 'woman2@admin.ad', 'woman2@admin.ad', 'woman2@admin.ad', 'woman2@admin.ad', 1, 'qu3rl69eg80gwo4o0gc8sooow4gwkwg', 'fG/Vy2yvzKHkBNjMlRvIRlsVobZk66C67cx2oTIXvlF4YhVWytcPPcFKtXoJzM5bbtJDrs3aT3uki2S/eVUzbA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman2firstname', 'woman2lasttname', '2015-01-31', 'married', 'woman'),
(5, 2, 1, 'woman3@admin.ad', 'woman3@admin.ad', 'woman3@admin.ad', 'woman3@admin.ad', 1, 'f6azshtujcowkk0wc4488ggwcck0o80', 'fprScIRxJeis8HpEnKQVcXRtH8s4MPYF177wOrT4Xzr+uLJZ85pnWGjvZG2GDgfqOsmabYoLUr0klIh42vq9Zw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman3firstname', 'woman3lasttname', '2015-01-31', 'not_marrie', 'woman'),
(6, 2, 1, 'woman4@admin.ad', 'woman4@admin.ad', 'woman4@admin.ad', 'woman4@admin.ad', 1, 'dsmsgko6qv40w88swgcwwwsoossgws8', 'F3VZT9LZx1WoP0+plDmsGWDiHzG6vhDNQ7dSmNC+fRIxuScpJqfdYu7kRlzBaFyU3jbRyCEcx4r0RITb4/Vfrw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman4firstname', 'woman4lasttname', '2015-01-31', 'not_marrie', 'woman'),
(7, 2, 1, 'woman5@admin.ad', 'woman5@admin.ad', 'woman5@admin.ad', 'woman5@admin.ad', 1, 'cabvalr88eo80www484w00ggkso0wkg', 'chA9PxivVn2gc8v8Xhwi33Ad9O0gjhkSJEroUE2UgAS2JZlBhBYYwzkKqW4CY5gY+MHWsaXpekvCXaI5bp87uw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman5firstname', 'woman5lasttname', '2015-01-31', 'married', 'woman'),
(8, 2, 1, 'woman6@admin.ad', 'woman6@admin.ad', 'woman6@admin.ad', 'woman6@admin.ad', 1, 'rwqhu4ci3j4kcgwco448s48wggkwsw0', 'gziMP0tjLG5g38oMRWRR2IZaWH41Fnn4vAy/BGhO9siG65wJDSu+YJxXD/wFWYuxREVaLPtujY8KZFFzAYqm5A==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman6firstname', 'woman6lasttname', '2015-01-31', 'married', 'woman'),
(9, 2, 1, 'woman7@admin.ad', 'woman7@admin.ad', 'woman7@admin.ad', 'woman7@admin.ad', 1, 'r08f4sb9f6884ck0oo88w4ckss048cs', 'n8zlXedCvk+kozHg51drhUuzUJLeoqEn0idb/rZOhltoXvGA8/rifuDO3i01fF0oVzxgfj6JW1LoTZzNeVYAdg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman7firstname', 'woman7lasttname', '2015-01-31', 'married', 'woman'),
(10, 2, 1, 'woman8@admin.ad', 'woman8@admin.ad', 'woman8@admin.ad', 'woman8@admin.ad', 1, 'o9q0zq8hzfkgosw8kk4ow4sgwoscw8c', 'iKL/6YiB6LTpTbSxDpyHBoa99TxHlQmjc4Ykk52OWt3LsQP8Za9XP5lZRum+OR03pVkRFdeufWBbveGLtC22hQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman8firstname', 'woman8lasttname', '2015-01-31', 'married', 'woman'),
(11, 2, 1, 'woman9@admin.ad', 'woman9@admin.ad', 'woman9@admin.ad', 'woman9@admin.ad', 1, 'e76uleb7p80s0skgkgws8ws0wo0sogo', '3UltpgoCKr3BhCb9aCqUSrpOtk22U5Kvn1MV05ms4cg0HD0+A1Dd2xN02ehak994WuMdJtJZ24RDyp27xKN65g==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman9firstname', 'woman9lasttname', '2015-01-31', 'not_marrie', 'woman'),
(12, 2, 1, 'woman10@admin.ad', 'woman10@admin.ad', 'woman10@admin.ad', 'woman10@admin.ad', 1, 'rdhlvoe0ixw40oc44s8gc4k4s4k8o00', 'ueBWVl4Iie7mVUq2Lc1BittwDulEcy+uD8LpnsZBVVp+b1uJZ1qLLmED0tdmI/zHggHY8du0v+CZIOxLf1whrg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman10firstname', 'woman10lasttname', '2015-01-31', 'not_marrie', 'woman'),
(13, 2, 1, 'woman11@admin.ad', 'woman11@admin.ad', 'woman11@admin.ad', 'woman11@admin.ad', 1, 'pxgx3ubbnbkc4sg8w80kogscksgkks0', '/tkNYOUClQ+cD31YI0DJTj6YzqQE0SSHrhBr8wAdS2AuKt552c5HV8F3Ns/ceEnNXDF2n9bKfUnJOw68Hr/OuA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman11firstname', 'woman11lasttname', '2015-01-31', 'not_marrie', 'woman'),
(14, 2, 1, 'woman12@admin.ad', 'woman12@admin.ad', 'woman12@admin.ad', 'woman12@admin.ad', 1, 'a4r66vqrrbc4cgwg04gow0swog8g4s4', '+bSzffxcmszb1keb3PFAnIz2Pyt4/ljPz0tY5tbsaUOGQvlDRl/wA41PgvsUztot/UHx7RfxwLHX24LXwAAU+A==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman12firstname', 'woman12lasttname', '2015-01-31', 'not_marrie', 'woman'),
(15, 2, 1, 'woman13@admin.ad', 'woman13@admin.ad', 'woman13@admin.ad', 'woman13@admin.ad', 1, 'jteltzhzdog08ogs8cgk040wo0cskkw', 'BHlrtRVnUAxTU7oVzVlINpPxQXfcEeRkPTTCKK8FGCErutfPdw8r/SfwgSAgCOhLGu8qtBXUrveB3vuMm19QEw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman13firstname', 'woman13lasttname', '2015-01-31', 'married', 'woman'),
(16, 2, 1, 'woman14@admin.ad', 'woman14@admin.ad', 'woman14@admin.ad', 'woman14@admin.ad', 1, 'ahh78u8b1mo0s88woc8g0w04cggc4ow', '88zDAnPvcd8t3jMZ9t45zxqrFRgmc4GDCU9CfIRRnPiuL7y2q5djHzN+C/vOHIbtDeuWYSXVCbExvdJRBcVS6A==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman14firstname', 'woman14lasttname', '2015-01-31', 'married', 'woman'),
(17, 2, 1, 'woman15@admin.ad', 'woman15@admin.ad', 'woman15@admin.ad', 'woman15@admin.ad', 1, 'sox7go3ie2sgwoo4sgo8w48cwkgw4wc', 'KJs1aTQHIdJH5pxv5t1csfGa7nlAlZvoQRosQ4zzOfbzjMIwi9z1aXEyutjQ9LixwHsnTZs8/GBcGIZTfLXEkg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman15firstname', 'woman15lasttname', '2015-01-31', 'married', 'woman'),
(18, 2, 1, 'woman16@admin.ad', 'woman16@admin.ad', 'woman16@admin.ad', 'woman16@admin.ad', 1, '5wxp9pkd6z4sssgowgokowg0o4c8s8c', 'Ct7ci+E1+cdf9Ix7hnIJbKx5j8BOdgmUHz/ZTbIx+vOBuOJhNzz57KuCeMMgUbS581Rd/S9kr+RFyGXEM6uvHw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman16firstname', 'woman16lasttname', '2015-01-31', 'married', 'woman'),
(19, 2, 1, 'woman17@admin.ad', 'woman17@admin.ad', 'woman17@admin.ad', 'woman17@admin.ad', 1, '3k1zu07orwe8ocgww0osc44ksoog84g', 'muTTvdg/c/rGOls0vRQzQ6nP5YvWxreO177P5mfECZ5coLT3CclXd6rHOhrti0g1yBiRV8hNkmK9BurJlu8O/g==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman17firstname', 'woman17lasttname', '2015-01-31', 'married', 'woman'),
(20, 2, 1, 'woman18@admin.ad', 'woman18@admin.ad', 'woman18@admin.ad', 'woman18@admin.ad', 1, 'lvzp6aovwvkockwwcwk004cwsggoggg', 'ue9IhQr6/QgRYoBqrAAHdzRB7fGZoD83LJPFG8+OqApfXi6dWDECuJPACkDPpwoa/81VrEebKI5BWokX/PzPmw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman18firstname', 'woman18lasttname', '2015-01-31', 'married', 'woman'),
(21, 2, 1, 'woman19@admin.ad', 'woman19@admin.ad', 'woman19@admin.ad', 'woman19@admin.ad', 1, '7ng6661w2jk08ksk4gg8ws0ws0kgkk8', '2fp8BWO8rw66PUI3ikbosKzM2/tppAlHC733B+vTr2Kr1zqDelBK2CQUrPJoL3TkZrI5S+bj7U3hwVii1u5C2g==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman19firstname', 'woman19lasttname', '2015-01-31', 'married', 'woman'),
(22, 2, 1, 'woman20@admin.ad', 'woman20@admin.ad', 'woman20@admin.ad', 'woman20@admin.ad', 1, 'mooxl7yle40ok0c8w48c0soo8wcsswk', '3HH0DBK34KKKk1emzBC9E+xJBPDJa01PF04x8goTMron6Sa/86M785RtaTf9HguEN8KJ549c7wPxS2+ksedPvA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman20firstname', 'woman20lasttname', '2015-01-31', 'married', 'woman'),
(23, NULL, NULL, 'man1@admin.ad', 'man1@admin.ad', 'man1@admin.ad', 'man1@admin.ad', 1, '50fjltbjq7wgcwockcoo0c44sw8o4gs', 'HwtDqZMpmLPZ2po5Sf8TYWDN2qBdLNm2MA/HZVxUC9WR2RcyiaZO1Csvm7wLZeq7OMU52vsRxxNV4AvdsLaSyQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(24, NULL, NULL, 'man2@admin.ad', 'man2@admin.ad', 'man2@admin.ad', 'man2@admin.ad', 1, '9gtpgomapw4ckcsw4wgc0k44cgkssws', '62k/1o17vwTbjAYJ7oTuRFHVFa50uxBHeG7q2uSO+rGbgVfhW+EeWLK0ujo3o8zt0vZFsPDn0zLp97L1okyHow==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(25, NULL, NULL, 'man3@admin.ad', 'man3@admin.ad', 'man3@admin.ad', 'man3@admin.ad', 1, '1sjd6l8al9ogw0cwsscs00kcsws0kkk', 'ZRvGlyUBuEsi0DNBU8MZj/RpGBrpr+WwWU/sfw76t1E4UkUaiam3L38iMddrGvrdyOh6kq1uoNXYlLNtLqAR2Q==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(26, NULL, NULL, 'man4@admin.ad', 'man4@admin.ad', 'man4@admin.ad', 'man4@admin.ad', 1, '3liei576ayw4ksgc0sw4w0k8gww0ksc', 'Zpo9fZDTYbsb2LOGyxrJXsXYq99pfDBk02hfeA4A+qDDoDQpAEzlSSQ83v5cIx3UYkx6RzKFH6RIy/koMrW/LA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(27, NULL, NULL, 'man5@admin.ad', 'man5@admin.ad', 'man5@admin.ad', 'man5@admin.ad', 1, 'ktkrmznc028c84kcos00804480skk88', '8qBoNH+acJX/RP5bY1q9rasyTpGabS0N0nubVQZFEXZRd9bo2iJcUYeFukTOHYUXTBKOxbhgMWeGbdRFVQBIiA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(28, NULL, NULL, 'man6@admin.ad', 'man6@admin.ad', 'man6@admin.ad', 'man6@admin.ad', 1, 'd3i5b512u884okowo0w0gkckww4gco0', 'b9zSz2GawnSxMHFr1LYPzxyq+1xoGhzHE6KfDqufSh4JPJaIGGPh9xHKxxMbjf1ycinX+XpJmU/Zvpb7ohIcYA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(29, NULL, NULL, 'man7@admin.ad', 'man7@admin.ad', 'man7@admin.ad', 'man7@admin.ad', 1, 'hm947e6ui680cgwwo0sww04gswsg80o', 'bNaBv8bwy3H2pEQqFR+6Mmmk3g0YjpOWt+5ud7hErcKd53Dm/EztHoBqg7t70yYRm7LLWTlBTQDTB2S4jyatWg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(30, NULL, NULL, 'man8@admin.ad', 'man8@admin.ad', 'man8@admin.ad', 'man8@admin.ad', 1, '4x6ohcn4uug4c0k0sgkcokkgks4scwk', 'pNeYQ7k0JhxI+V3wH6Ut9fTnaLWbwM7SZ2ZkP2MUgbllLcnIOP2KsatmBt+Oh2/kOCi74d5IXWxFd0cx99mgNA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(31, NULL, NULL, 'man9@admin.ad', 'man9@admin.ad', 'man9@admin.ad', 'man9@admin.ad', 1, '2y5gisn1lnuoggkgk84s0o8kckckwcc', 'WV7VESn+WJOk0El2s58i1lNSKntRJKqH5btLXgKYtZ77VX7oOupYGZKesmOg+tthc9hv7gWYqhMSNGuDAJxlug==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(32, NULL, NULL, 'man10@admin.ad', 'man10@admin.ad', 'man10@admin.ad', 'man10@admin.ad', 1, 'dctpxe94jbc40oooog0c80o0o4ogcsk', '3h8mp/SLeeOBl0CbTo7REINvDN3x/KpOrUeCm5rbRghJMqQ1rFCB2ixq0Fd6nPSf9b2n59YUEUFz/otddvbEmQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(33, NULL, NULL, 'man11@admin.ad', 'man11@admin.ad', 'man11@admin.ad', 'man11@admin.ad', 1, 'h9bs99iprtsg0w4840g4s48ws88s0c8', '+P2vWFxivkhmglyt10ASkjN1s5VTVYFt70tqNMRaN3nWwCdiHYJqGSuo0FUbMsTNaRjITmqFa24fRKJMSOcsSw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(34, NULL, NULL, 'man12@admin.ad', 'man12@admin.ad', 'man12@admin.ad', 'man12@admin.ad', 1, 'td6vlao6dfk0c84sw0ws888wgkso4k4', '7IwBgJyfF5CHTC4tdHfsM/6r/jMHhZ2r4JXxgv6rs1tVZlCgg6XYfBzhk5tha3hj1j+b6i6hCGcN1Q0+1zWYQQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(35, NULL, NULL, 'man13@admin.ad', 'man13@admin.ad', 'man13@admin.ad', 'man13@admin.ad', 1, 'runpyjwcw408gcwss04wsk4g0c48gow', 'QCF363Ru+kSeqbzVQ6C/EHN/l7IRpyre0ox53Z5CwJ18vEcr1yN0jTrReKbey06raIvN/yus2hVlIBqO3ALW2Q==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(36, NULL, NULL, 'man14@admin.ad', 'man14@admin.ad', 'man14@admin.ad', 'man14@admin.ad', 1, '7uwvkpakqc8wsgw48wscg0o4ccgc0gk', '6VISqTrJ4u+mp/ZOfa32xfdpVyLW/UBjpU609QJP+9711TRElOgYD6+486Gm0l+mxdbGSoF01JexhLaRwo35JQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(37, NULL, NULL, 'man15@admin.ad', 'man15@admin.ad', 'man15@admin.ad', 'man15@admin.ad', 1, 'pc6koqavtuokg044gskgk8os08840oc', 'dQUTRdOmrr7CLiFtIrE1lLcfozS2gVzrkwlQQjV15hFQsJMG5uvGST+A6J46kKSgwT3eVNT0ILPMppRHIivYnA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(38, NULL, NULL, 'man16@admin.ad', 'man16@admin.ad', 'man16@admin.ad', 'man16@admin.ad', 1, 'ez020vd615w0k4cs0c0wocwsswocwg0', '144p4WpwX7ZmUls7i5TQYLT3tXchkD2gzacGEK7MwrbEUNbBLR9fTa9+0XlWDizXQSPoXjZE28zuBNXVd81Gmw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(39, NULL, NULL, 'man17@admin.ad', 'man17@admin.ad', 'man17@admin.ad', 'man17@admin.ad', 1, '7wpxtdrbq9wk8cswwocko44wwc880ko', '2Z6Awt426pLPZEVqzvTMo73HG7cRVJ5E9NQNwfuXVP1D7yD3ExEipDdg3MQp2xDXnvXkfcligAvIaJEL/DdBgQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(40, NULL, NULL, 'man18@admin.ad', 'man18@admin.ad', 'man18@admin.ad', 'man18@admin.ad', 1, 'k1dxfhfr0w0wkcwwgcck8kcskkss48w', 'wdLv2BUfkzkm9AMihIlhspGxGFuUulhbUvRPoBI/TWtxHiYB2fsbCgAOOHCFt6cS+WIIOGhq55CirHpumkrJBQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(41, NULL, NULL, 'man19@admin.ad', 'man19@admin.ad', 'man19@admin.ad', 'man19@admin.ad', 1, '3a59gygd3xk48ccgs8g0ogook44cos4', 'nVOQAs+UMlD79IWroeDcehL0JK+Xx05TPJaC8GafxyapWOtSg8YpBnLBXhbPFS42fIH4NT+nMVc1IyazCKujbg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man'),
(42, NULL, NULL, 'man20@admin.ad', 'man20@admin.ad', 'man20@admin.ad', 'man20@admin.ad', 1, '9bgq0436x80skw8w00wsowc88o8gkow', 'e6E9k8l45KvSHbZZLkEQ/OFl/GklX1ZtpGD63DqLqIyBL/oO1Y9tKe0SCdgfQcVtV+DopQsabBMt/VzKrBmbFA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', NULL, NULL, NULL, NULL, 'man');

-- --------------------------------------------------------

--
-- Структура таблицы `fos_woman`
--

CREATE TABLE IF NOT EXISTS `fos_woman` (
  `id` int(11) NOT NULL,
  `breast` int(10) unsigned DEFAULT NULL,
  `waist` int(10) unsigned DEFAULT NULL,
  `hips` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `fos_woman`
--

INSERT INTO `fos_woman` (`id`, `breast`, `waist`, `hips`) VALUES
(3, 110, 91, 142),
(4, 120, 100, 183),
(5, 150, 144, 96),
(6, 195, 128, 177),
(7, 97, 172, 139),
(8, 93, 136, 168),
(9, 97, 173, 110),
(10, 185, 145, 98),
(11, 134, 195, 148),
(12, 117, 188, 156),
(13, 140, 155, 131),
(14, 122, 190, 177),
(15, 175, 145, 126),
(16, 161, 146, 182),
(17, 167, 191, 146),
(18, 138, 141, 115),
(19, 157, 193, 125),
(20, 175, 190, 102),
(21, 133, 153, 144),
(22, 179, 121, 166);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `sex` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_day` date DEFAULT NULL,
  `marital_status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `breast` int(10) unsigned DEFAULT NULL,
  `waist` int(10) unsigned DEFAULT NULL,
  `hips` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  KEY `IDX_8D93D649F92F3E70` (`country_id`),
  KEY `IDX_8D93D6498BAC62AF` (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=43 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `country_id`, `city_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `sex`, `first_name`, `last_name`, `birth_day`, `marital_status`, `type`, `body_type`, `breast`, `waist`, `hips`) VALUES
(1, NULL, NULL, 'admin@admin.ad', 'admin@admin.ad', 'admin@admin.ad', 'admin@admin.ad', 1, 'maw96b5cyv404cosc44kcwo084g844s', 'ARK7DJbrlncXtusJNaa1O6BkW8aKCCXrgh5UgrfYhZ4oKE+KIiK1jfJI6PUdiufQsd0oRaV4YLzkNId3m80uiQ==', '2015-02-02 11:38:50', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'user', NULL, NULL, NULL, NULL),
(2, NULL, NULL, 'user@user.us', 'user@user.us', 'user@user.us', 'user@user.us', 1, 's6l6qqqdcggc8gks48swkgo4ck8wswg', 'gkX/smP3xTKgVi0PLIAZxElOti20wG5VMz6SNyOZjXnbONtTmxFLJj2ZtHPkzlm9SGPV/sNrOjytHu46M+Xh8g==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'user', NULL, NULL, NULL, NULL),
(3, 2, 1, 'man1@admin.ad', 'man1@admin.ad', 'man1@admin.ad', 'man1@admin.ad', 1, '3fqdi1eqvfgg0gkk4k8w4gg8g4ocwo', '0i6tNbRlujuMFTlSl5OpELtEl1MpO0c/JMfxnTgMLig4KCtMuAb4PN67aSnsV1v0NDSYf1bNfgM+uCLYyWWpiw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man1firstname', 'man1lasttname', '2015-02-02', 'not_marrie', 'man', 'muscular', NULL, NULL, NULL),
(4, 2, 1, 'man2@admin.ad', 'man2@admin.ad', 'man2@admin.ad', 'man2@admin.ad', 1, '1scgrc4h7a1wso4g0kccko40kcw0s44', 'bng3mjLnd+wfkCwmyeDqFVeq90nqs/ENkt5VV2CKBa7+OClvaKn3IbUjJK36F0uCHqMIuo11gYa/wtbuQKE8dQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man2firstname', 'man2lasttname', '2015-02-02', 'not_marrie', 'man', 'thick', NULL, NULL, NULL),
(5, 2, 1, 'man3@admin.ad', 'man3@admin.ad', 'man3@admin.ad', 'man3@admin.ad', 1, 'jqbo1um7mm84okwg0kskkw8o000s480', 'KxHAWKVP1nB+i6GuvKyP5FWYFgl7VZzpJmqYsrWkft1dP9chwXjf9ueoI/9XYxysB2cs3q+MjB7rAi4kF9GcSA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man3firstname', 'man3lasttname', '2015-02-02', 'married', 'man', 'thin', NULL, NULL, NULL),
(6, 2, 1, 'man4@admin.ad', 'man4@admin.ad', 'man4@admin.ad', 'man4@admin.ad', 1, 'lxd9ys19rxcgwwgs04w0w48g8s0444o', 'x3ET3R+TszRMZkoQ7yteOZVmvkyHF2agDh3fYOiKD3bWXJBn0eXD7bgpQ+0AVSaRA1bCUqeGz/Sg+5jkDhJhbQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man4firstname', 'man4lasttname', '2015-02-02', 'married', 'man', 'thin', NULL, NULL, NULL),
(7, 2, 1, 'man5@admin.ad', 'man5@admin.ad', 'man5@admin.ad', 'man5@admin.ad', 1, 'qzqum5ap7280kskkswgwcgg0wokcwo', 'PgSvzcv5r7DM3anI4vOExs10dIzwQMg2TQtREpnpeSyE/mtZDROnf2OIgL0B39gQcevNBTHJxJ4/+pKxcb2GWA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man5firstname', 'man5lasttname', '2015-02-02', 'not_marrie', 'man', 'muscular', NULL, NULL, NULL),
(8, 2, 1, 'man6@admin.ad', 'man6@admin.ad', 'man6@admin.ad', 'man6@admin.ad', 1, 'oxlnyvr13q8g84gs0sogksgog4k4wsg', 'cGeOufVCMFW0agF5dRE6enG23V2Y7p66BaFM6Tl+Ct2pQtbnrSZ1reQt/WctzmuXcj/OUR4zsBTOnJGbyHGHTA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man6firstname', 'man6lasttname', '2015-02-02', 'married', 'man', 'thin', NULL, NULL, NULL),
(9, 2, 1, 'man7@admin.ad', 'man7@admin.ad', 'man7@admin.ad', 'man7@admin.ad', 1, '9c2sa0gs0dgk04s04gcgscso0csgw8c', 'A2pCL47/qY5PhuSCmQbjEYQVJ8VYnBlZqp6xSnG/v4YFwsUHVZcU1f9+6096FTGlVOuMOK3U3wi5Kj5qPgA8AQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man7firstname', 'man7lasttname', '2015-02-02', 'married', 'man', 'thin', NULL, NULL, NULL),
(10, 2, 1, 'man8@admin.ad', 'man8@admin.ad', 'man8@admin.ad', 'man8@admin.ad', 1, 'nmsd3vegaas0g8g4s8s80c4cs408kgk', 'T/JKqIlY26NxZqJxKCcHMP8qBlJ6ogmokr2B7zc5pUQnEfNNvG/7P2dMcyNbeybIBFGmklqx1VV2mukNgRDYAA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man8firstname', 'man8lasttname', '2015-02-02', 'married', 'man', 'muscular', NULL, NULL, NULL),
(11, 2, 1, 'man9@admin.ad', 'man9@admin.ad', 'man9@admin.ad', 'man9@admin.ad', 1, '39iedqbm098gocg4ookogc8sggos80g', 'TdN37O88kgZZDwGyuNDD19izIY/B1nYkfYDkEpno1LQpkQ2BOZ/bDHHcbA79Q2jmY8fpI/3NuCVNVf76zH3e/g==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man9firstname', 'man9lasttname', '2015-02-02', 'not_marrie', 'man', 'thin', NULL, NULL, NULL),
(12, 2, 1, 'man10@admin.ad', 'man10@admin.ad', 'man10@admin.ad', 'man10@admin.ad', 1, 'g5jdaoicofksks88c8w8ocgkc8swo0c', '6NTwhfYkNlWTJ6yr7DE6k7bsfhqdcgy9iQzgJsGSlOcX9dC7CTtKM3n297e75+OWWsnKXxoM+3cECT7EHZbgDQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man10firstname', 'man10lasttname', '2015-02-02', 'married', 'man', 'thick', NULL, NULL, NULL),
(13, 2, 1, 'man11@admin.ad', 'man11@admin.ad', 'man11@admin.ad', 'man11@admin.ad', 1, '3e90qgawjtwk0c00008gocsws4sksw8', 'zdkdE6RSyno6VIGLm7Dr9ZCV3ZwPFqO/4wMF1cWaa78OKKApqXlPrHRB/rwKdP5H5mO7HWiwtsHQcV19y94XVw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man11firstname', 'man11lasttname', '2015-02-02', 'married', 'man', 'thick', NULL, NULL, NULL),
(14, 2, 1, 'man12@admin.ad', 'man12@admin.ad', 'man12@admin.ad', 'man12@admin.ad', 1, '2ga2mmwb9zk0cgogw00sw0kw4ooso4g', 'zxrktigr/fjIIzVTi8GM/EgS82hmN5ijgqmpgVpXjQSteQ2aA/YJtKe5BJZvTwJ/w9ZG89LDmvGpBpYgA8cQ5g==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man12firstname', 'man12lasttname', '2015-02-02', 'married', 'man', 'muscular', NULL, NULL, NULL),
(15, 2, 1, 'man13@admin.ad', 'man13@admin.ad', 'man13@admin.ad', 'man13@admin.ad', 1, 'jizodaiejnk08csoggc80g4go8kwc8g', 'oVXYxyfU6dy3WsM6ShWd3/SnncWjDrFjgFx+s38kMQa7nIRF8PpDyeQnBtvcBD2/oU+Gx6I4hP91mycfTNX7aw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man13firstname', 'man13lasttname', '2015-02-02', 'not_marrie', 'man', 'muscular', NULL, NULL, NULL),
(16, 2, 1, 'man14@admin.ad', 'man14@admin.ad', 'man14@admin.ad', 'man14@admin.ad', 1, 'r6lbjp6jh0gkggokoww04sg4ww8go00', 'mqAJTWTlIyW4gIzflR7b0P7XWnb7k/tI98Xo+SzSbIJJKCvkobda8B/T0kPKAcya0gm+fBLa7FvGPty6cJzk7w==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man14firstname', 'man14lasttname', '2015-02-02', 'not_marrie', 'man', 'thin', NULL, NULL, NULL),
(17, 2, 1, 'man15@admin.ad', 'man15@admin.ad', 'man15@admin.ad', 'man15@admin.ad', 1, '7yesvbraegow4wkkw48k0soo4s00g48', 'oH/+GkRilmD+aRnd+ltPh1A9AVA2qXFCgZeoJl/t5EDRLhfewJFjvtvBuYaq1bWly1j+fyQ3QhS/YOZWDeGzyg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man15firstname', 'man15lasttname', '2015-02-02', 'not_marrie', 'man', 'thick', NULL, NULL, NULL),
(18, 2, 1, 'man16@admin.ad', 'man16@admin.ad', 'man16@admin.ad', 'man16@admin.ad', 1, 'dd46l1buavk8088s40w0gwws4g0s0cc', 'dhkgPiTMrL6fIdWRoGGCmG59vV1+QpLtQey03yoBDgTzayFxEcRNPiJRhejYP7255vMPOUXwb+uK90p/9DEczQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man16firstname', 'man16lasttname', '2015-02-02', 'not_marrie', 'man', 'muscular', NULL, NULL, NULL),
(19, 2, 1, 'man17@admin.ad', 'man17@admin.ad', 'man17@admin.ad', 'man17@admin.ad', 1, 'qjir7l47zio80c84ckc0gc88gc0o4wg', 'JNHi29yAf2xvkuAtbDiHliYWcCmhcTmmaAZRkQbeJL1Xbk4Gc5MCMtpGlAjVTzsmYnTbhPiajNVqraRS7lh9Ng==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man17firstname', 'man17lasttname', '2015-02-02', 'married', 'man', 'muscular', NULL, NULL, NULL),
(20, 2, 1, 'man18@admin.ad', 'man18@admin.ad', 'man18@admin.ad', 'man18@admin.ad', 1, 'o14f8s53lfkwgc8o0ko8cc8w44kw0s8', 'L5sSl2AmUAAZqHDcLbaYXb5pvtEkaRzVKT62cdxhVxUGuWx5gTAmbmgOt5Ma3EMvdDY1l4F6JGe4qU/GANYEdg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man18firstname', 'man18lasttname', '2015-02-02', 'not_marrie', 'man', 'muscular', NULL, NULL, NULL),
(21, 2, 1, 'man19@admin.ad', 'man19@admin.ad', 'man19@admin.ad', 'man19@admin.ad', 1, 'a4booyf7ohs0oos0k8444wsgcgook8o', 'M/+UJhYyawRbI81TytdQxp2dgAIYFWwjfn6lMIuTaCC1HkAgXDsjimHnoEQ+YQGwQ0AMhjNvts6aZRBCi5iCbA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man19firstname', 'man19lasttname', '2015-02-02', 'not_marrie', 'man', 'muscular', NULL, NULL, NULL),
(22, 2, 1, 'man20@admin.ad', 'man20@admin.ad', 'man20@admin.ad', 'man20@admin.ad', 1, 'ke6ldmodpdcos4googcckwwcwsoss8o', 'gt8atDjXjwgQkB69RuNbKXXQGZSLVs9IpfbBK4wPI2nc5sJWt79zqOOZt+ghLg+h5Zj8D4viHIWIbblZQvjkYQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'man', 'man20firstname', 'man20lasttname', '2015-02-02', 'married', 'man', 'thin', NULL, NULL, NULL),
(23, 2, 1, 'woman1@admin.ad', 'woman1@admin.ad', 'woman1@admin.ad', 'woman1@admin.ad', 1, '70alk73j0fwg84s00kck4swwkkgccws', 'VlrIDSiBVh1Nuu7AulYJ77pZinMGaiwEe+29/Ur9NIl1Ncc6xjZbg2hoW5bus+Gcb6jv1/6J0TeoWoc8HLGf/A==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman1firstname', 'woman1lasttname', '2015-02-02', 'married', 'woman', NULL, 115, 161, 103),
(24, 2, 1, 'woman2@admin.ad', 'woman2@admin.ad', 'woman2@admin.ad', 'woman2@admin.ad', 1, '9wlncnsa814o4gkccwc888s4k4goswk', 'Q0yizu9iou7XLUFPEy7GxH8SQD/CtrlL+Uj0tBHKEmT+IdTfRLm6yd990fjwFRS5Guf/LHwix3km9vulwAlRXw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman2firstname', 'woman2lasttname', '2015-02-02', 'married', 'woman', NULL, 198, 118, 174),
(25, 2, 1, 'woman3@admin.ad', 'woman3@admin.ad', 'woman3@admin.ad', 'woman3@admin.ad', 1, 'po8563buaysk80scw0kwwg4s0kc4www', 'ifaoxOm4H2OXqckoxeAR329Htn34gM2zyuSpBFmAQ/PDLRiYwhSXTnsSJP18+I+HVa9sV6fqMJs7opqFcwbI2Q==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman3firstname', 'woman3lasttname', '2015-02-02', 'married', 'woman', NULL, 172, 139, 147),
(26, 2, 1, 'woman4@admin.ad', 'woman4@admin.ad', 'woman4@admin.ad', 'woman4@admin.ad', 1, 'bsf218x0zyo8wgk44088w8kgcsgo440', 'JvEQqkSLzO7UEX97kn0dKo/Y98un1o5B5PsSwIZYZERQPWR4tgYN7wCnfMamt4Q8YZ8FyVNr8TImVuDSHl/37Q==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman4firstname', 'woman4lasttname', '2015-02-02', 'married', 'woman', NULL, 122, 113, 136),
(27, 2, 1, 'woman5@admin.ad', 'woman5@admin.ad', 'woman5@admin.ad', 'woman5@admin.ad', 1, 'ruzl52647o0c4s40k00g4g0ckk0sogo', 'lFiAa4OclQf1J1f0KfzBzsmnA8kXedPceAPKv5wtBbu1JpqCCz2ypLeS88Q2q+EuVYO9T8K7v0xqqI9mUbDwWA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman5firstname', 'woman5lasttname', '2015-02-02', 'married', 'woman', NULL, 91, 144, 164),
(28, 2, 1, 'woman6@admin.ad', 'woman6@admin.ad', 'woman6@admin.ad', 'woman6@admin.ad', 1, 'tbl6bx4onxw8wg4kc4cg4o8occgc48c', 'QFRc8gFdkyoaW2En8aY9+t3HgVV66kQCNCeAb+/i1HtGSv4kh+FrbaIr584s3GTC7hwNtVk+6YG6gey5FaBBqg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman6firstname', 'woman6lasttname', '2015-02-02', 'married', 'woman', NULL, 95, 114, 188),
(29, 2, 1, 'woman7@admin.ad', 'woman7@admin.ad', 'woman7@admin.ad', 'woman7@admin.ad', 1, 'e9w1lnvuhw08wcw4okg8488w8g0k404', 'fsMFgecg+2LcWVWBCopJV0eg5+V2Fwvu8nE36dn57nRBQklqUBqE46t0uRdGExWyUHf9nBKnwEG/A1xCh7zqpA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman7firstname', 'woman7lasttname', '2015-02-02', 'married', 'woman', NULL, 125, 161, 198),
(30, 2, 1, 'woman8@admin.ad', 'woman8@admin.ad', 'woman8@admin.ad', 'woman8@admin.ad', 1, '1au70emh15j4wc0s48so4sso8s4co84', '3tNPpfaNChSR9F9rJWCREBBtvuXc8o22eJOwT/0bdGU6hziIadfsY5qPOLkspki4GJxR1/39JxYsOq+Y9Fmg/g==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman8firstname', 'woman8lasttname', '2015-02-02', 'married', 'woman', NULL, 114, 136, 141),
(31, 2, 1, 'woman9@admin.ad', 'woman9@admin.ad', 'woman9@admin.ad', 'woman9@admin.ad', 1, 'leiuilr5vpcw4wokscws4swkgk04844', 'OrjSFugvNHnAx8X3gU1kpeR1aisotbX44oNt4Jrh9PmAAU2zgY2LnRvial1hYDvm5esRtHISm6jVOrOwuNbYKQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman9firstname', 'woman9lasttname', '2015-02-02', 'married', 'woman', NULL, 123, 154, 140),
(32, 2, 1, 'woman10@admin.ad', 'woman10@admin.ad', 'woman10@admin.ad', 'woman10@admin.ad', 1, 'omcples3kxcogk04c4ks40w80k0wckc', 'o7wtIvoyeb2Ct85wSEc/FHC4lnT/xXtAKd/wts/BftAOcVO46N0ZKBFGqg+peN4rY8kpZ3y9UwwhW3KRfWGKqA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman10firstname', 'woman10lasttname', '2015-02-02', 'not_marrie', 'woman', NULL, 163, 120, 105),
(33, 2, 1, 'woman11@admin.ad', 'woman11@admin.ad', 'woman11@admin.ad', 'woman11@admin.ad', 1, '6weumh9xft8ooockckwkc0go8g448gs', 'JPrydoZrPdRVP/pP1uSGP6hhKsDDAJ/yXOcKBk2QB3OBAoNeYdxfsX7C2v6lriESWjWwshP7mfdxPlRQF1zzKA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman11firstname', 'woman11lasttname', '2015-02-02', 'not_marrie', 'woman', NULL, 122, 190, 90),
(34, 2, 1, 'woman12@admin.ad', 'woman12@admin.ad', 'woman12@admin.ad', 'woman12@admin.ad', 1, '3a08bt2dlaio8oscgwowo08kwcgwscw', 'nr5gT9RgGQbukvhZ4e7JnBz0gfmClLtgygSPrnMnGb0kW+H1f7mVtSEZ5WhKXwhD30m1GHeVWWvrekDYzM5ujg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman12firstname', 'woman12lasttname', '2015-02-02', 'married', 'woman', NULL, 186, 109, 177),
(35, 2, 1, 'woman13@admin.ad', 'woman13@admin.ad', 'woman13@admin.ad', 'woman13@admin.ad', 1, '84ezjc6wengowcoosowggs8w4ww044g', 'zYN7ukXE0xSctHAoep3US2Fg+lZhyHrS3M+tIDal3Q/GGCCX5wASdEXiFQh/Yf88mP3FG8In2sk1CqLvepeItA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman13firstname', 'woman13lasttname', '2015-02-02', 'married', 'woman', NULL, 98, 121, 99),
(36, 2, 1, 'woman14@admin.ad', 'woman14@admin.ad', 'woman14@admin.ad', 'woman14@admin.ad', 1, '8t3q9v4ehmgw4ocoo0ggo8c0g0g0sc0', '4O9MdgiKPvZtFlcu/AAdEBAYTAMRBOVQgA1EtRATFfFpyWdefA5TxirB9MH1GiZ8fBZa1H10UTzIg462ZeMEFQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman14firstname', 'woman14lasttname', '2015-02-02', 'married', 'woman', NULL, 123, 150, 170),
(37, 2, 1, 'woman15@admin.ad', 'woman15@admin.ad', 'woman15@admin.ad', 'woman15@admin.ad', 1, 'ievgqdkp00ocs8s00ws0scg8gwg4kw8', '1DEKZCmnJe5sC4FAyp7FxdimJfUhHIuIMsM8g12ns6U4+8ZICW5JYKLBOjSk9731PzmNQMNRPjlcpWjHTMuxzQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman15firstname', 'woman15lasttname', '2015-02-02', 'not_marrie', 'woman', NULL, 199, 169, 126),
(38, 2, 1, 'woman16@admin.ad', 'woman16@admin.ad', 'woman16@admin.ad', 'woman16@admin.ad', 1, 'ppbypp118tckssocssoco00s4scgs44', 'O4z3/Mm8M0v8jxTGT5AmhKEZSlVD8WzKnK/DP4WF0jdnT53Htm1WCFzS0gRDNWdGq1eAXNEkEeP+hOtt/ubeAw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman16firstname', 'woman16lasttname', '2015-02-02', 'not_marrie', 'woman', NULL, 173, 138, 98),
(39, 2, 1, 'woman17@admin.ad', 'woman17@admin.ad', 'woman17@admin.ad', 'woman17@admin.ad', 1, 'mqpw949sf28wgc0s04sckg0kgcg48sw', 'lE3VAnttlhQLNmBXLbw/tn1JjV49mJ5mjfQwblXA+sUtDzWnjuma0vg9O5CooeuMNXhlUoij21sqt69DPv5tgg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman17firstname', 'woman17lasttname', '2015-02-02', 'not_marrie', 'woman', NULL, 106, 188, 123),
(40, 2, 1, 'woman18@admin.ad', 'woman18@admin.ad', 'woman18@admin.ad', 'woman18@admin.ad', 1, '8aukczl0psw0ocoksg8owc8w4wkgsko', 'w52HOPI1SRPGU7FuZpDMFpxTCFuYliRm/JHdvj98MffGT02cOUv3BQgk49htJ6gFdhXCeoruByFvN+cRZkcWIQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman18firstname', 'woman18lasttname', '2015-02-02', 'not_marrie', 'woman', NULL, 136, 107, 97),
(41, 2, 1, 'woman19@admin.ad', 'woman19@admin.ad', 'woman19@admin.ad', 'woman19@admin.ad', 1, 'gzrys2dl8jwook0gw0oog8go80kw804', '4rfAdPGEo3ZgXprGD3F2h/pWLxqthf22XNijnFqELh/xTggaQhuymLY2kbr1pxDniV1E0U5kUqM3MvPi+dHPrw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman19firstname', 'woman19lasttname', '2015-02-02', 'married', 'woman', NULL, 147, 184, 121),
(42, 2, 1, 'woman20@admin.ad', 'woman20@admin.ad', 'woman20@admin.ad', 'woman20@admin.ad', 1, 'de3x3j7dk1cscsksw8sowwc0scg8ow0', 'KUFlHUNxnt5mxHd/lql2DlUkyUodhzbIaBOH1onRwDXrm9vxq09PCkA754vnK8+EE+xjFaKAhiujX44/yD1ESw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'woman', 'woman20firstname', 'woman20lasttname', '2015-02-02', 'married', 'woman', NULL, 141, 131, 155);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `FK_2D5B0234F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);

--
-- Ограничения внешнего ключа таблицы `fos_admin`
--
ALTER TABLE `fos_admin`
  ADD CONSTRAINT `FK_AECFD468BF396750` FOREIGN KEY (`id`) REFERENCES `fos_person` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `fos_man`
--
ALTER TABLE `fos_man`
  ADD CONSTRAINT `FK_3757D00FBF396750` FOREIGN KEY (`id`) REFERENCES `fos_person` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `fos_woman`
--
ALTER TABLE `fos_woman`
  ADD CONSTRAINT `FK_5EB7BB43BF396750` FOREIGN KEY (`id`) REFERENCES `fos_person` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D6498BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`),
  ADD CONSTRAINT `FK_8D93D649F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
