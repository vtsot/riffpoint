<?php

namespace Riffpoint\AdminBundle\Form\Type\Man;

use Admingenerated\RiffpointAdminBundle\Form\BaseManType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;
use Riffpoint\AdminBundle\Form\Type\EventListener\UserEventSubscriber;

/**
 * EditType
 */
class EditType extends BaseEditType
{
    
    /**
     * Построить форму
     * @param FormBuilderInterface $builder,
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // построить форму родителем
        parent::buildForm($builder, $options);
        
        // добавить EventSubscriber формы
        $builder->addEventSubscriber(new UserEventSubscriber());
    }
    
}
