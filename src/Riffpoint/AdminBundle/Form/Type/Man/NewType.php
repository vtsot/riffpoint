<?php

namespace Riffpoint\AdminBundle\Form\Type\Man;

use Admingenerated\RiffpointAdminBundle\Form\BaseManType\NewType as BaseNewType;
use Symfony\Component\Form\FormBuilderInterface;
use Riffpoint\AdminBundle\Form\Type\EventListener\UserEventSubscriber;

/**
 * NewType
 */
class NewType extends BaseNewType
{
    
    /**
     * Построить форму
     * @param FormBuilderInterface $builder,
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // построить форму родителем
        parent::buildForm($builder, $options);
        
        // добавить EventSubscriber формы
        $builder->addEventSubscriber(new UserEventSubscriber());
    }
    
}
