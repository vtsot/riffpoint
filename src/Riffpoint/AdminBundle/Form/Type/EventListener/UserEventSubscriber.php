<?php

namespace Riffpoint\AdminBundle\Form\Type\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Riffpoint\AdminBundle\Entity\Model\PersonInterface;

/**
 * UserEventSubscriber
 * Обработчик событий для формы пользователей
 */
class UserEventSubscriber implements EventSubscriberInterface
{    
    
    /**
     * получить массив событий
     * @return array 
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::POST_SUBMIT => 'postSubmit');
    }
    
    /**
     * Установить пользователю новый пароль пароль
     * @param PersonInterface   $user       - пользователь
     * @param string            $password   - новый пароль
     */
    protected function setUserNewPassword(PersonInterface $user, $password)
    {
        // сбросить старый пароль
        $user->setPassword(null);
        // установить новый пароль
        $user->setPlainPassword($password);
    }
    
    /**
     * postSubmit
     * @param FormEvent $event
     */
    public function postSubmit(FormEvent $event)
    {
        
        // получить пользователя
        $user = $event->getData();
        
        // получить форму
        $form = $event->getForm();
        
        // если пользователь не сохранен в БД
        // создание нового пользователя
        if (!$user->getId()) {
            // установить данные пользователя
            $user
                ->setUsername($form->get('email')->getData())
                ->setEnabled(true);
            
            // устанавливаемый пароль
            // если пароль пришел из формы устанавливаем пароль 
            $password = ($form->has('password') && $form->get('password')->getData())
                // пароль из формы 
                ? $form->get('password')->getData()
                // случайный пароль для нового пользователя
                : substr(sha1(uniqid().time()), 0, 5);

            // установить пароль
            $this->setUserNewPassword($user, $password);
        }
        // обновление данных пользователя
        // если в форме есть виджет пароль
        // и если пользователь ввел новый пароль
        elseif ($form->has('password') && $form->get('password')->getData()) {
            // установить пароль
            $this->setUserNewPassword($user, $form->get('password')->getData());
        }
    }
    
}
