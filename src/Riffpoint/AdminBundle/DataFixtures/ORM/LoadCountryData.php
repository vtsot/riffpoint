<?php

namespace Riffpoint\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Riffpoint\AdminBundle\Entity\Country;

/**
 * LoadCountryData
 * Фикстуры стран 
 */
class LoadCountryData extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 0;
    }
    
    /**
     * Загрузить фикстуры
     * @param ObjectManager $manager 
     */
    public function load(ObjectManager $manager)
    {
        
        // создать страну
        $countryUSA = new Country();
        $countryUSA->setName('USA');
        $this->setReference('countryUSA', $countryUSA);
        $manager->persist($countryUSA);
        
        // создать страну
        $countryUA = new Country();
        $countryUA->setName('UA');
        $this->setReference('countryUA', $countryUA);
        $manager->persist($countryUA);
        
        // создать страну
        $countryRU = new Country();
        $countryRU->setName('RU');
        $this->setReference('countryRU', $countryRU);
        $manager->persist($countryRU);

        // создать страну
        $countryPL = new Country();
        $countryPL->setName('PL');
        $this->setReference('countryPL', $countryPL);
        $manager->persist($countryPL);
        
        // сохранить 
        $manager->flush();
    }
    
}
