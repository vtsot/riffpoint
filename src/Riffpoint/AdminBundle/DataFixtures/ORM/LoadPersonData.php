<?php

namespace Riffpoint\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Riffpoint\AdminBundle\Entity\Man;
use Riffpoint\AdminBundle\Entity\Woman;

/**
 * LoadPersonData
 * Фикстуры зарегистрированных пользователей Man и Woman
 */
class LoadPersonData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 2;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    /**
     * Валидировать список арнументов передаваемых в метод
     * @param mixed, валидируемые объекты, через запятую
     * @throw \Exception - ошибка валидации
     * @return true - валидация пройдена успешно
     */
    public function processValidArgs()
    {
        // получить массив валидируемых объектов
        $args = func_get_args();
        if (!$args) {
            throw new \Exception("No one object.");
        }
        
        // получить валидатор
        $validator = $this->container->get('validator');
        
        // валидировать каждый эдемент массива
        foreach($args as $obj) {
            // валидировать объект
            $errors = $validator->validate($obj);
            if ($errors->count()) {
                foreach($errors as $error) {
                    throw new \Exception("Validator error: ".get_class($obj).". field: \"".$error->getPropertyPath()."\". ". $error->getMessage());
                }
            }
        }
        
        // валидация пройдена успешно
        return true;
    }
    
    /**
     * Получить случайное мсемейное положение \
     * @return string
     */
    public function getRandomMaritalStatus()
    {
        $maritalStatus = array('married', 'not_married');
        $maritalStatusKey = array_rand($maritalStatus);
        return $maritalStatus[$maritalStatusKey];
    }

    /**
     * Получить случайное телосложение
     * @return string
     */
    public function getRandomBodyType()
    {
        $bodyType = array('thin', 'muscular', 'thick');
        $bodyTypeKey = array_rand($bodyType);
        return $bodyType[$bodyTypeKey];
    }

    /**
     * Загрузить фикстуры
     * @param ObjectManager $manager 
     */
    public function load(ObjectManager $manager)
    {
        
        // загрузить фикстуры Man
        $this->LoadManData($manager);
        
        // загрузить фикстуры Woman
        $this->LoadWomanData($manager);
    }
    
    /**
     * Загрузить фикстуры Man
     * @param ObjectManager $manager 
     */
    public function LoadManData(ObjectManager $manager)
    {
        // создать несколько записенй
        for($i=1; $i<=20; $i++) {
            // создать пользователя
            $prefix = 'man'.$i;    
            $person = new Man();
            $person->setUsername($prefix.'@admin.ad')
                ->setEmail($prefix.'@admin.ad')
                ->setPlainPassword('1')
                ->setEnabled(true)
                ->setFirstName($prefix.'firstname')
                ->setLastName($prefix.'lasttname')
                ->setCountry($this->getReference('countryUA'))
                ->setCity($this->getReference('cityKharkov'))
                ->setBirthDay(new \DateTime())
                ->setMaritalStatus($this->getRandomMaritalStatus())
                ->setBodyType($this->getRandomBodyType());
            // валидировать и запомнить пользователя для сохранения
            $this->processValidArgs($person);
            $manager->persist($person);
        }
        
        // сохранить 
        $manager->flush();
    }
    
    /**
     * Загрузить фикстуры Woman
     * @param ObjectManager $manager 
     */
    public function LoadWomanData(ObjectManager $manager)
    {
        // создать несколько записенй
        for($i=1; $i<=20; $i++) {
            // создать пользователя
            $prefix = 'woman'.$i;    
            $person = new Woman();
            $person->setUsername($prefix.'@admin.ad')
                ->setEmail($prefix.'@admin.ad')
                ->setPlainPassword('1')
                ->setEnabled(true)
                ->setFirstName($prefix.'firstname')
                ->setLastName($prefix.'lasttname')
                ->setCountry($this->getReference('countryUA'))
                ->setCity($this->getReference('cityKharkov'))
                ->setBirthDay(new \DateTime())
                ->setMaritalStatus($this->getRandomMaritalStatus())
                ->setWaist(rand(90, 200))
                ->setHips(rand(90, 200))
                ->setBreast(rand(90, 200));
            // валидировать и запомнить пользователя для сохранения
            $this->processValidArgs($person);
            $manager->persist($person);
        }
        
        // сохранить 
        $manager->flush();
    }
    
}
