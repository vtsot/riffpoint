<?php

namespace Riffpoint\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Riffpoint\AdminBundle\Entity\City;

/**
 * LoadCityData
 * Фикстуры городов 
 */
class LoadCityData extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1;
    }
    
    /**
     * Загрузить фикстуры
     * @param ObjectManager $manager 
     */
    public function load(ObjectManager $manager)
    {
        
        // создать город 
        $cityKharkov = new City();
        $cityKharkov
            ->setName('Kharkov')
            ->setCountry($this->getReference('countryUA'));
        // запомнить город
        $this->setReference('cityKharkov', $cityKharkov);
        $manager->persist($cityKharkov);
        
        // создать город 
        $cityPoltava = new City();
        $cityPoltava
            ->setName('Poltava')
            ->setCountry($this->getReference('countryUA'));
        // запомнить город
        $this->setReference('cityPoltava', $cityPoltava);
        $manager->persist($cityPoltava);
        
        // создать город 
        $cityKiev = new City();
        $cityKiev
            ->setName('Kiev')
            ->setCountry($this->getReference('countryUA'));
        // запомнить город
        $this->setReference('cityKiev', $cityKiev);
        $manager->persist($cityKiev);
        
        
        
        // создать город 
        $cityMoscow = new City();
        $cityMoscow
            ->setName('Moscow')
            ->setCountry($this->getReference('countryRU'));
        // запомнить город
        $this->setReference('cityMoscow', $cityMoscow);
        $manager->persist($cityMoscow);
        
        // создать город 
        $cityBelgorod = new City();
        $cityBelgorod
            ->setName('Belgorod')
            ->setCountry($this->getReference('countryRU'));
        // запомнить город
        $this->setReference('cityBelgorod', $cityBelgorod);
        $manager->persist($cityBelgorod);
        
        // создать город 
        $cityKursk = new City();
        $cityKursk
            ->setName('Kursk')
            ->setCountry($this->getReference('countryRU'));
        // запомнить город
        $this->setReference('cityKursk', $cityKursk);
        $manager->persist($cityKursk);
        
        
        // создать город 
        $cityWarsaw = new City();
        $cityWarsaw
            ->setName('Warsaw')
            ->setCountry($this->getReference('countryPL'));
        // запомнить город
        $this->setReference('cityWarsaw', $cityWarsaw);
        $manager->persist($cityWarsaw);
        
        
        // сохранить 
        $manager->flush();
    }
    
}
