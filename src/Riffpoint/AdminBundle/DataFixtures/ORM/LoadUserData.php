<?php

namespace Riffpoint\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Riffpoint\AdminBundle\Entity\User;

/**
 * LoadUserData
 * Фикстуры пользователей
 */
class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 0;
    }
    
    /**
     * Загрузить фикстуры
     * @param ObjectManager $manager 
     */
    public function load(ObjectManager $manager)
    {
        
        // создать админа
        $admin = new User();
        $admin
            ->setUsername('admin@admin.ad')
            ->setEmail('admin@admin.ad')
            ->setPlainPassword('1')
            ->addRole('ROLE_ADMIN')
            ->setEnabled(true);
        // запомнить пользователя для сохранения
        $manager->persist($admin);
        
        
        // создать пользователя
        $user = new User();
        $user
            ->setUsername('user@user.us')
            ->setEmail('user@user.us')
            ->setPlainPassword('1')
            ->setEnabled(true);
        // запомнить пользователя для сохранения
        $manager->persist($user);
        
        // сохранить 
        $manager->flush();
    }
    
}
