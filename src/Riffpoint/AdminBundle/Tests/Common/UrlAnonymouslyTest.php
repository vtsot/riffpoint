<?php

namespace Riffpoint\AdminBundle\Tests\Common;

/**
 * UrlAnonymouslyTest
 * Тест не авторизованных URL
 * Тест Url не требующих авторизации
 */
class UrlAnonymouslyTest extends \Symfony\Bundle\FrameworkBundle\Test\WebTestCase
{
    
    /**
     * получить массив проверяемых url
     * @return array
     */
    public function providerUrls()
    {
        return array(
            
            array('/login'),
            
        );
    }

    /** 
     * @dataProvider providerUrls 
     */
    public function testUrl($url)
    {
        // получить клиента
        $client = static::createClient();
        $client->request('GET', $url);
        // проверить флаг доступности ссылки
        $errorMessage = "Code: ". $client->getResponse()->getStatusCode(). " url: ". $url;
        $this->assertTrue($client->getResponse()->isOk(), $errorMessage);
    }
    
}
