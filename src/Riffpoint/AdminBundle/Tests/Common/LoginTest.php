<?php

namespace Riffpoint\AdminBundle\Tests\Common;

/**
 * LoginTest
 * Тест авторизации пользователя
 */
class LoginTest extends \Riffpoint\AdminBundle\Tests\RiffpointAdminTests
{
    
    /**
     * Данные авторизации
     * @return array 
     */
    public function loginProvider()
    {
        return array(
            // array(результат авторизации,    'логин', 'пароль'),
            array(false,    '', ''),
            array(false,    'wrong-username', 'wrong-password'),
            array(false,    'admin@admin.ad', ''),
            array(true,     'admin@admin.ad', 'admin'),
            array(true,     'man1@admin.ad', 'man1'),
            array(true,     'woman1@admin.ad ', 'woman1'),
        );
    }
    
    /**
     * Тестирование не правильной авторизации
     * @dataProvider    loginProvider
     */
    public function testLogin($isOk, $username, $password)
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
        
        // проверить контроллер авторизации
        $this->assertEquals(
            'FOS\UserBundle\Controller\SecurityController::loginAction', 
            $client->getRequest()->attributes->get('_controller')
        );
        
        // ввести данные пользователя
        $form = $crawler->filter('.login_content > form')->form(array(
            '_username' => $username,
            '_password' => $password,
        ));
        
        // клик вход в систему
        $client->submit($form);
        
        // првоерить контроллер обработчика авторизации
        $this->assertEquals(
            'FOS\UserBundle\Controller\SecurityController::checkAction', 
            $client->getRequest()->attributes->get('_controller')
        );
        
        // перенаправить пользователя
        $client->followRedirect();
        
        // если не правилная авторизация
        if (!$isOk) {
            
            // проверить результат авторизации
            // рользователь должен быть перенаправлен на форму авторизации
            $this->assertEquals(
                'FOS\UserBundle\Controller\SecurityController::loginAction', 
                $client->getRequest()->attributes->get('_controller')
            );
            
        } else {
            // авторизация пройдена успешно 

            // атрибуты перенаправления
            $requestAttributes = $client->getRequest()->attributes->all();
            // проверить наличие параметров перенаправления
            $this->assertArrayHasKey('_controller', $requestAttributes);
            $this->assertContains(
                $requestAttributes['_controller'], 
                array(
                    'Riffpoint\AdminBundle\Controller\RedirectAfterLoginController::redirectAfterLoginAction',
                    'FOS\UserBundle\Controller\SecurityController::loginAction',
            ));

            // проверить наличие параметров перенаправления
            $this->assertArrayHasKey('_route', $requestAttributes);
            $this->assertContains(
                $requestAttributes['_route'],
                array(
                    'fos_user_security_login', 
                    'redirect_after_login',
            ));
        }
    }
    
}
