<?php

namespace Riffpoint\AdminBundle\Tests\Common;

/**
 * UrlSuccessTest
 * Проверить доступность Url 
 */
class UrlSuccessTest extends \Symfony\Bundle\FrameworkBundle\Test\WebTestCase
{
    
    /**
     * получить массив проверяемых url
     * @return array
     */
    public function providerUrls()
    {
        return array(
            
            array('/'),
            
            array('/login'),
            array('/logout'),
            array('/redirect-after-login'),
            
            array('/admin/user'),
            array('/admin/user/new'),
            array('/admin/user/filter'),
            
            array('/admin/country'),
            array('/admin/country/new'),
            array('/admin/country/filter'),
            
            array('/admin/city'),
            array('/admin/city/new'),
            array('/admin/city/filter'),
            
            array('/admin/men'),
            array('/admin/men/new'),
            array('/admin/men/filter'),

            array('/admin/women'),
            array('/admin/women/new'),
            array('/admin/women/filter'),

        );
    }

    /** 
     * @dataProvider providerUrls 
     */
    public function testPageIsSuccessful($url)
    {
        $client = static::createClient();
        $client->request('GET', $url);
        
        // флаг доступности ссылки 
        $isSuccessful = ($client->getResponse()->isSuccessful() || $client->getResponse()->isRedirection())
            ? true 
            : false;
        
        // проверить флаг доступности ссылки
        $this->assertTrue($isSuccessful);
    }
    
}
