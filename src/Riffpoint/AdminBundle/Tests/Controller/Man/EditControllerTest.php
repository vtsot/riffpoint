<?php

namespace Riffpoint\AdminBundle\Tests\Controller\Man;

/**
 * EditControllerTest
 * Тест редактировать запись
 */
class EditControllerTest extends AbstractMan
{

    /**
     * {@inheritDoc}
     * @return array массив данных редактируемой записи
     */
    public function testController()
    {
        // получить редактируемую запись
        $editRow = $this->getEditRow();
        $this->assertTrue(isset($editRow['id']) && $editRow['id']);

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/men/'.$editRow['id'].'/edit');
        
        // проверить контроллер
        $this->assertEquals(
            'Riffpoint\AdminBundle\Controller\Man\EditController::indexAction', 
            $client->getRequest()->attributes->get('_controller')
        );
        
        // проверить ID
        $this->assertEquals(
            $editRow['id'], 
            $client->getRequest()->attributes->get('pk')
        );
        
        // вернуть редактируемую запись 
        return $editRow;
    }
    
    /**
     * Тестирование редактирования записи
     * @depends testController
     */
    public function testEdit($editRow)
    {   
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/men/'.$editRow['id'].'/edit');
        $crawler = $client->getCrawler();
        
        // префикс 
        $prefix = 'test-upd-man'.date('YmdHis');
        
        // получить форму
        $form = $crawler->selectButton('save')->form(array(
            'edit_man[firstName]'       => 'firstName'.$prefix,
            'edit_man[lastName]'        => 'lastName'.$prefix,
            'edit_man[birthDay]'        => '24 Jan 2000',
            'edit_man[maritalStatus]'   => 'not_married',
            'edit_man[bodyType]'        => 'muscular',
        ));
        
        // клик создание новой записи
        $client->submit($form);
        $crawler = $client->getCrawler();
        
        // Получить данные после редактирования
        $updatedRow = $this->getEditRow();
        
        // сравнить реультат обновления
        $this->assertEquals($editRow['id'], $updatedRow['id']);
        $this->assertTrue($editRow['firstName'] != $updatedRow['firstName']);
        $this->assertTrue($editRow['lastName'] != $updatedRow['lastName']);
    }
    
}
