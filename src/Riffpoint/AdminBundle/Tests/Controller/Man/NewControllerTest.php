<?php

namespace Riffpoint\AdminBundle\Tests\Controller\Man;

/**
 * NewControllerTest
 * Тест добавления новой записи
 */
class NewControllerTest extends AbstractMan
{
    
    /**
     * {@inheritDoc}
     */
    public function testController()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/men/new');
        
        // проверить контроллер
        $this->assertEquals(
            'Riffpoint\AdminBundle\Controller\Man\NewController::indexAction', 
            $client->getRequest()->attributes->get('_controller')
        );
    }
    
    /**
     * Тестирование добавления записи
     * @depends testController
     */
    public function testNew()
    {
        // получить кол-во перед тестированием
        $countBefore = $this->getTotalRows();
        
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/men/new');
        $crawler = $client->getCrawler();
        
        // получить город 
        $city = $this->getFirstCity();
        
        // префикс 
        $prefix = 'new_test_man'.($countBefore+1);
        
        // получить форму
        $form = $crawler->selectButton('save')->form(array(
            'new_man[email]'            => $prefix.'@email.em',
            'new_man[firstName]'        => 'firstName'.$prefix,
            'new_man[lastName]'         => 'lastName'.$prefix,
            'new_man[country]'          => $city['country']['id'],
            'new_man[city]'             => $city['id'],
            'new_man[birthDay]'         => '20 Jan 2000',
            'new_man[maritalStatus]'    => 'married',
            'new_man[bodyType]'         => 'thin',
        ));

        // клик создание новой записи
        $client->submit($form);
        
        // получить кол-во после теста
        $countAfter = $this->getTotalRows();
        
        // проверить кол-во
        $this->assertGreaterThan($countBefore, $countAfter);
    }
    
}
