<?php

namespace Riffpoint\AdminBundle\Tests\Controller\Man;

/**
 * AbstractMan
 * Абстрактный класс тест справочника Man
 */
abstract class AbstractMan extends \Riffpoint\AdminBundle\Tests\RiffpointAdminTests
{
    
    /**
     * Тест контроллера
     */
    abstract public function testController();

    /**
     * Получить первую запись город
     * @return array - массив данных 
     */
    public function getFirstCity()
    {
        return static::getEntityManager()
            ->createQueryBuilder()
            ->select('city, country')
            ->from('RiffpointAdminBundle:City', 'city')
            ->innerJoin('city.country', 'country')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
    }
    
    /**
     * Получить первую запись
     * @return array - массив данных 
     */
    public function getEditRow()
    {
        $email = 'man1@admin.ad';
        return static::getEntityManager()
            ->createQueryBuilder()
            ->select('q')
            ->from('RiffpointAdminBundle:Man', 'q')
            ->where('q.email = :email')->setParameter('email', $email)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
    }
    
    /**
     * Получить счетчик кол-ва записей
     * @return integer
     */
    public function getTotalRows()
    {
        // запрос получения кол-ва заказов
        $result = static::getEntityManager()
            ->createQueryBuilder()
            ->select('COUNT(DISTINCT q)')
            ->from('RiffpointAdminBundle:Man', 'q')
            ->getQuery()
            ->getOneOrNullResult();
        
        // вернуть результат счетчика
        return ($result) ? $result[1] : 0;
    }
    
}
