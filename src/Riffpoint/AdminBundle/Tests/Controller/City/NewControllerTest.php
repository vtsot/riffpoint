<?php

namespace Riffpoint\AdminBundle\Tests\Controller\City;

/**
 * NewControllerTest
 * Тест добавления новой записи
 */
class NewControllerTest extends AbstractCity
{
    
    /**
     * {@inheritDoc}
     */
    public function testController()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/city/new');
        
        // проверить контроллер
        $this->assertEquals(
            'Riffpoint\AdminBundle\Controller\City\NewController::indexAction', 
            $client->getRequest()->attributes->get('_controller')
        );
    }
    
    /**
     * Тестирование добавления записи
     * @depends testController
     */
    public function testNew()
    {
        // получить кол-во перед тестированием
        $countBefore = $this->getTotalRows();
        
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/city/new');
        $crawler = $client->getCrawler();
        
        // префикс 
        $prefix = 'test-new-city'.($countBefore+1);
        
        // получить форму
        $form = $crawler->selectButton('save')->form(array(
            'new_city[country]' => $this->getFirstCountry()['id'],
            'new_city[name]'    => $prefix,
        ));
        
        // клик создание новой записи
        $client->submit($form);
        
        // получить кол-во после теста
        $countAfter = $this->getTotalRows();
        
        // проверить кол-во
        $this->assertGreaterThan($countBefore, $countAfter);
    }
    
}
