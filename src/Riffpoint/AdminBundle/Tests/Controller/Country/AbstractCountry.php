<?php

namespace Riffpoint\AdminBundle\Tests\Controller\Country;

/**
 * AbstractCountry
 * Абстрактный класс тест справочника стран
 */
abstract class AbstractCountry extends \Riffpoint\AdminBundle\Tests\RiffpointAdminTests
{
    
    /**
     * Тест контроллера
     */
    abstract public function testController();

    /**
     * Получить первую запись
     * @return array - массив данных 
     */
    public function getEditRow()
    {
        return static::getEntityManager()
            ->createQueryBuilder()
            ->select('q')
            ->from('RiffpointAdminBundle:Country', 'q')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
    }
    
    /**
     * Получить счетчик кол-ва записей
     * @return integer
     */
    public function getTotalRows()
    {
        // запрос получения кол-ва заказов
        $result = static::getEntityManager()
            ->createQueryBuilder()
            ->select('COUNT(DISTINCT q)')
            ->from('RiffpointAdminBundle:Country', 'q')
            ->getQuery()
            ->getOneOrNullResult();
        
        // вернуть результат счетчика
        return ($result) ? $result[1] : 0;
    }
    
}
