<?php

namespace Riffpoint\AdminBundle\Tests\Controller\Country;

/**
 * NewControllerTest
 * Тест добавления новой записи
 */
class NewControllerTest extends AbstractCountry
{
    
    /**
     * {@inheritDoc}
     */
    public function testController()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/country/new');
        
        // проверить контроллер
        $this->assertEquals(
            'Riffpoint\AdminBundle\Controller\Country\NewController::indexAction', 
            $client->getRequest()->attributes->get('_controller')
        );
    }
    
    /**
     * Тестирование добавления записи
     * @depends testController
     */
    public function testNew()
    {
        // получить кол-во перед тестированием
        $countBefore = $this->getTotalRows();
        
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/country/new');
        $crawler = $client->getCrawler();
        
        // префикс 
        $prefix = 'test-new-country'.($countBefore+1);
        
        // получить форму
        $form = $crawler->selectButton('save')->form(array(
            'new_country[name]' => $prefix,
        ));
        
        // клик создание новой записи
        $client->submit($form);
        
        // получить кол-во после теста
        $countAfter = $this->getTotalRows();
        
        // проверить кол-во
        $this->assertGreaterThan($countBefore, $countAfter);
    }
    
}
