<?php

namespace Riffpoint\AdminBundle\Tests\Controller\Country;

/**
 * EditControllerTest
 * Тест редактировать запись
 */
class EditControllerTest extends AbstractCountry
{

    /**
     * {@inheritDoc}
     * @return array массив данных редактируемой записи
     */
    public function testController()
    {
        // получить редактируемую запись
        $editRow = $this->getEditRow();
        $this->assertTrue(isset($editRow['id']) && $editRow['id']);

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/country/'.$editRow['id'].'/edit');
        
        // проверить контроллер
        $this->assertEquals(
            'Riffpoint\AdminBundle\Controller\Country\EditController::indexAction', 
            $client->getRequest()->attributes->get('_controller')
        );
        
        // проверить ID
        $this->assertEquals(
            $editRow['id'], 
            $client->getRequest()->attributes->get('pk')
        );
        
        // вернуть редактируемую запись 
        return $editRow;
    }
    
    /**
     * Тестирование редактирования записи
     * @depends testController
     */
    public function testEdit($editRow)
    {   
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/country/'.$editRow['id'].'/edit');
        $crawler = $client->getCrawler();
        
        // префикс 
        $prefix = 'test-upd-country'.date('YmdHis');
        
        // получить форму
        $form = $crawler->selectButton('save')->form(array(
            'edit_country[name]' => $prefix,
        ));
        
        // клик создание новой записи
        $client->submit($form);
        $crawler = $client->getCrawler();
        
        // Получить данные после редактирования
        $updatedRow = $this->getEditRow();
        
        // сравнить реультат обновления
        $this->assertEquals($editRow['id'], $updatedRow['id']);
        $this->assertTrue($editRow['name'] != $updatedRow['name']);
    }
    
}
