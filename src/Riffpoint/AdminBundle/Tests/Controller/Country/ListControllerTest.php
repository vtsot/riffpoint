<?php

namespace Riffpoint\AdminBundle\Tests\Controller\Country;

/**
 * ListControllerTest
 * Тест список позиций
 */
class ListControllerTest extends AbstractCountry
{
    
    /**
     * {@inheritDoc}
     */
    public function testController()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/country/');
        
        // проверить контроллер
        $this->assertEquals(
            'Riffpoint\AdminBundle\Controller\Country\ListController::indexAction', 
            $client->getRequest()->attributes->get('_controller')
        );
    }
    
    /**
     * Тестирование отображения списка 
     * @depends testController
     */
    public function testList()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/country/');
        $crawler = $client->getCrawler();
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("('.$this->getTotalRows().')")')->count();
        $this->assertTrue($hasHeader > 0);
    }
    
}
