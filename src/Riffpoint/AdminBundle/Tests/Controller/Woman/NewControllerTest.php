<?php

namespace Riffpoint\AdminBundle\Tests\Controller\Woman;

/**
 * NewControllerTest
 * Тест добавления новой записи
 */
class NewControllerTest extends AbstractWoman
{
    
    /**
     * {@inheritDoc}
     */
    public function testController()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/women/new');
        
        // проверить контроллер
        $this->assertEquals(
            'Riffpoint\AdminBundle\Controller\Woman\NewController::indexAction', 
            $client->getRequest()->attributes->get('_controller')
        );
    }
    
    /**
     * Тестирование добавления записи
     * @depends testController
     */
    public function testNew()
    {
        // получить кол-во перед тестированием
        $countBefore = $this->getTotalRows();
        
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/women/new');
        $crawler = $client->getCrawler();
        
        // получить город 
        $city = $this->getFirstCity();
        
        // префикс 
        $prefix = 'new_test_woman'.($countBefore+1);
        
        // получить форму
        $form = $crawler->selectButton('save')->form(array(
            'new_woman[email]'          => $prefix.'@email.em',
            'new_woman[firstName]'      => 'firstName'.$prefix,
            'new_woman[lastName]'       => 'lastName'.$prefix,
            'new_woman[country]'        => $city['country']['id'],
            'new_woman[city]'           => $city['id'],
            'new_woman[birthDay]'       => '20 Jan 2000',
            'new_woman[maritalStatus]'  => 'married',
            'new_woman[breast]'         => rand(90, 200),
            'new_woman[waist]'          => rand(90, 200),
            'new_woman[hips]'           => rand(90, 200),
        ));

        // клик создание новой записи
        $client->submit($form);
        
        // получить кол-во после теста
        $countAfter = $this->getTotalRows();
        
        // проверить кол-во
        $this->assertGreaterThan($countBefore, $countAfter);
    }
    
}
