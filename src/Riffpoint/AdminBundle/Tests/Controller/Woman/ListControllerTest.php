<?php

namespace Riffpoint\AdminBundle\Tests\Controller\Woman;

/**
 * ListControllerTest
 * Тест список позиций
 */
class ListControllerTest extends AbstractWoman
{
    
    /**
     * {@inheritDoc}
     */
    public function testController()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/women/');
        
        // проверить контроллер
        $this->assertEquals(
            'Riffpoint\AdminBundle\Controller\Woman\ListController::indexAction', 
            $client->getRequest()->attributes->get('_controller')
        );
    }
    
    /**
     * Тестирование отображения списка 
     * @depends testController
     */
    public function testList()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/women/');
        $crawler = $client->getCrawler();
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("('.$this->getTotalRows().')")')->count();
        $this->assertTrue($hasHeader > 0);
    }
    
}
