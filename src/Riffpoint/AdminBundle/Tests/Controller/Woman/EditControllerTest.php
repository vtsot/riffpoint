<?php

namespace Riffpoint\AdminBundle\Tests\Controller\Woman;

/**
 * EditControllerTest
 * Тест редактировать запись
 */
class EditControllerTest extends AbstractWoman
{

    /**
     * {@inheritDoc}
     * @return array массив данных редактируемой записи
     */
    public function testController()
    {
        // получить редактируемую запись
        $editRow = $this->getEditRow();
        $this->assertTrue(isset($editRow['id']) && $editRow['id']);

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/women/'.$editRow['id'].'/edit');
        
        // проверить контроллер
        $this->assertEquals(
            'Riffpoint\AdminBundle\Controller\Woman\EditController::indexAction', 
            $client->getRequest()->attributes->get('_controller')
        );
        
        // проверить ID
        $this->assertEquals(
            $editRow['id'], 
            $client->getRequest()->attributes->get('pk')
        );
        
        // вернуть редактируемую запись 
        return $editRow;
    }
    
    /**
     * Тестирование редактирования записи
     * @depends testController
     */
    public function testEdit($editRow)
    {   
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/women/'.$editRow['id'].'/edit');
        $crawler = $client->getCrawler();
        
        // префикс 
        $prefix = 'test-upd-woman'.date('YmdHis');
        
        // получить форму
        $form = $crawler->selectButton('save')->form(array(
            'edit_woman[firstName]'     => 'firstName'.$prefix,
            'edit_woman[lastName]'      => 'lastName'.$prefix,
            'edit_woman[birthDay]'      => '24 Jan 2000',
            'edit_woman[maritalStatus]' => 'not_married',
            'edit_woman[breast]'        => rand(90, 200),
            'edit_woman[waist]'         => rand(90, 200),
            'edit_woman[hips]'          => rand(90, 200),
        ));
        
        // клик создание новой записи
        $client->submit($form);
        $crawler = $client->getCrawler();
        
        // Получить данные после редактирования
        $updatedRow = $this->getEditRow();
        
        // сравнить реультат обновления
        $this->assertEquals($editRow['id'], $updatedRow['id']);
        $this->assertTrue($editRow['firstName'] != $updatedRow['firstName']);
        $this->assertTrue($editRow['lastName'] != $updatedRow['lastName']);
    }
    
}
