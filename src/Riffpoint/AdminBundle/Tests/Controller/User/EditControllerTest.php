<?php

namespace Riffpoint\AdminBundle\Tests\Controller\User;

/**
 * EditControllerTest
 * Тест редактировать запись
 */
class EditControllerTest extends AbstractUser
{

    /**
     * {@inheritDoc}
     * @return array массив данных редактируемой записи
     */
    public function testController()
    {
        // получить редактируемую запись
        $editRow = $this->getEditRow();
        $this->assertTrue(isset($editRow['id']) && $editRow['id']);

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/user/'.$editRow['id'].'/edit');
        
        // проверить контроллер
        $this->assertEquals(
            'Riffpoint\AdminBundle\Controller\User\EditController::indexAction', 
            $client->getRequest()->attributes->get('_controller')
        );
        
        // проверить ID
        $this->assertEquals(
            $editRow['id'], 
            $client->getRequest()->attributes->get('pk')
        );
        
        // вернуть редактируемую запись 
        return $editRow;
    }
    
    /**
     * Тестирование редактирования записи
     * @depends testController
     */
    public function testEdit($editRow)
    {   
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/user/'.$editRow['id'].'/edit');
        $crawler = $client->getCrawler();
        
        // префикс 
        $prefix = 'test-upd-user'.date('YmdHis');
        
        // получить форму
        $form = $crawler->selectButton('save')->form(array(            
            'edit_user[sex]'            => 'man',
            'edit_user[firstName]'      => 'firstName'.$prefix,
            'edit_user[lastName]'       => 'lastName'.$prefix,
            'edit_user[birthDay]'       => '30 Jan 2001',
            'edit_user[maritalStatus]'  => 'not_married',
        ));
        
        // клик создание новой записи
        $client->submit($form);
        $crawler = $client->getCrawler();
        
        // Получить данные после редактирования
        $updatedRow = $this->getEditRow();
        
        // сравнить реультат обновления
        $this->assertEquals($editRow['id'], $updatedRow['id']);
        $this->assertTrue($editRow['firstName'] != $updatedRow['firstName']);
        $this->assertTrue($editRow['lastName'] != $updatedRow['lastName']);
    }
    
}
