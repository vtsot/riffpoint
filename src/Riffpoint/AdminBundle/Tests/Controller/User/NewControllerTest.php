<?php

namespace Riffpoint\AdminBundle\Tests\Controller\User;

/**
 * NewControllerTest
 * Тест добавления новой записи
 */
class NewControllerTest extends AbstractUser
{
    
    /**
     * {@inheritDoc}
     */
    public function testController()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/user/new');
        
        // проверить контроллер
        $this->assertEquals(
            'Riffpoint\AdminBundle\Controller\User\NewController::indexAction', 
            $client->getRequest()->attributes->get('_controller')
        );
    }
    
    /**
     * Тестирование добавления записи
     * @depends testController
     */
    public function testNew()
    {
        // получить кол-во перед тестированием
        $countBefore = $this->getTotalRows();
        
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/admin/user/new');
        $crawler = $client->getCrawler();
        
        // получить город 
        $city = $this->getFirstCity();
        
        // префикс 
        $prefix = 'new_test_user'.($countBefore+1);
        
        // получить форму
        $form = $crawler->selectButton('save')->form(array(
            'new_user[email]'               => $prefix.'@email.em',
            'new_user[password][first]'     => '1234',
            'new_user[password][second]'    => '1234',
            //  'new_user[roles]'               => array('ROLE_USER'),
            'new_user[sex]'                 => 'man',
            'new_user[firstName]'           => 'firstName'.$prefix,
            'new_user[lastName]'            => 'lastName'.$prefix,
            'new_user[country]'             => $city['country']['id'],
            'new_user[city]'                => $city['id'],
            'new_user[birthDay]'            => '30 Jan 2000',
            'new_user[maritalStatus]'       => 'married',
        ));

        // клик создание новой записи
        $client->submit($form);
        
        // получить кол-во после теста
        $countAfter = $this->getTotalRows();
        
        // проверить кол-во
        $this->assertGreaterThan($countBefore, $countAfter);
    }
    
}
