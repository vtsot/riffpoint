<?php

namespace Riffpoint\AdminBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * RedirectAfterLoginController
 * Перенаправление пользователя после авторизации
 */
class RedirectAfterLoginController extends Controller
{
    
    /**
     * @Route("/redirect-after-login", name="redirect_after_login")
     */
    public function redirectAfterLoginAction()
    {
        // получить пользователя
        $user = $this->getUser();
        
        // проверить пользователя 
        if ($user && $user instanceof UserInterface) {
            
            // проверить роль пользователя
            // если пользовтель админ
            if ($user->hasRole('ROLE_ADMIN')) {
                // todo: перенаправить пользователя 
                // на список Men
                return new RedirectResponse($this->generateUrl('Riffpoint_AdminBundle_Man_list'));
            }
            
            // todo: перенаправить пользователя 
            // на редактирование профайла
            // return new RedirectResponse($this->generateUrl('Riffpoint_AdminBundle_User_list'));
            
        }        
        
        // по умолчанию перенаправить пользователя 
        // на форму авторизации
        return new RedirectResponse($this->generateUrl('fos_user_security_login'));
    }    
    
}
