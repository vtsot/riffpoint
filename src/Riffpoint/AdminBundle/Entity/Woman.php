<?php

namespace Riffpoint\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class Woman extends Model\Person implements Model\WomanInterface
{
    
    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned":true})
     * @Assert\Range(min=0, max=255)
     * @Assert\NotBlank()
     */
    protected $breast;
    
    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned":true})
     * @Assert\Range(min=0, max=255)
     * @Assert\NotBlank()
     */
    protected $waist;
    
    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned":true})
     * @Assert\Range(min=0, max=255)
     * @Assert\NotBlank()
     */
    protected $hips;
    
    /**
     * {@inheritdoc}
     * @Assert\True(message="Incorrect value for sex.")
     */
    public function isValidSex()
    {
        return ($this->getSex() == 'woman');
    }
    
    /**
     * {@inheritdoc}
     */
    public function initialization()
    {
        $this->setSex('woman');
    }
    
    /**
     * {@inheritdoc}
     */
    public function setBreast($breast)
    {
        $this->breast = $breast;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getBreast()
    {
        return $this->breast;
    }

    /**
     * {@inheritdoc}
     */
    public function setWaist($waist)
    {
        $this->waist = $waist;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getWaist()
    {
        return $this->waist;
    }

    /**
     * {@inheritdoc}
     */
    public function setHips($hips)
    {
        $this->hips = $hips;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getHips()
    {
        return $this->hips;
    }

}
