<?php

namespace Riffpoint\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class Man extends Model\Person implements Model\ManInterface
{
    
    /**
     * Тип телосложения
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Assert\Choice(choices={"thin", "muscular", "thick"})
     * @Assert\NotBlank()
     */
    protected $bodyType;
    
    /**
     * {@inheritdoc}
     * @Assert\True(message="Incorrect value for sex.")
     */
    public function isValidSex()
    {
        return ($this->getSex() == 'man');
    }
    
    /**
     * {@inheritdoc}
     */
    public function initialization()
    {
        $this->setSex('man');
    }
    
    /**
     * {@inheritdoc}
     */
    public function setBodyType($bodyType)
    {
        $this->bodyType = $bodyType;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getBodyType()
    {
        return $this->bodyType;
    }
    
}
