<?php

namespace Riffpoint\AdminBundle\Entity\Model;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity()
 * @UniqueEntity(fields="email")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *      "man"   = "\Riffpoint\AdminBundle\Entity\Man", 
 *      "woman" = "\Riffpoint\AdminBundle\Entity\Woman", 
 *      "user"  = "\Riffpoint\AdminBundle\Entity\User"})
 */
abstract class Person extends BaseUser implements PersonInterface
{

    // подключить Trait админ-генератора
    // Проверяет, можно ли удалять объект
    use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;
    
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * Пол
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Assert\Choice(choices={"man", "woman"})
     * @Assert\NotBlank()
     */
    protected $sex;  
    
    /**
     * @var string $firstName
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(min="3", max="255")
     * @Assert\NotBlank()
     */
    protected $firstName;

    /**
     * @var string $lastName
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(min="3", max="255")
     * @Assert\NotBlank()
     */
    protected $lastName;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Riffpoint\AdminBundle\Entity\Country")
     * @ORM\JoinColumn(nullable=true)
     * @Assert\NotBlank()
     */
    protected $country;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Riffpoint\AdminBundle\Entity\City")
     * @ORM\JoinColumn(nullable=true)
     * @Assert\NotBlank()
     */
    protected $city;
    
    /**
     * @var Date - $birthDay день рождения
     * @ORM\Column(type="date", nullable=true)
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    protected $birthDay;
        
    /**
     * Семейное положение
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Assert\Choice(choices={"married", "not_married"})
     * @Assert\NotBlank()
     */
    protected $maritalStatus;
    
    /**
     * Проверяет, можно ли удалять объект
     * @return boolean
     */
    public function isUserValidForDelete()
    {
        // если пользователь superadmin
        if ($this->isSuperAdmin()) {
            // запрет удаления пользователя
            return false;
        }
        
        // пользователь не superadmin 
        // Проверяет, можно ли удалять объект
        return $this->isValidForDelete();
    }
    
    /**
     * Инициализация
     */
    public function initialization()
    {
        
    }
    
    /**
     * Конструктор 
     */
    public function __construct()
    {
        // вызвать конструктор родителя
        parent::__construct();
        $this->initialization();
    }

    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sex
     * @param string $sex
     * @return User
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
        return $this;
    }

    /**
     * Get sex
     * @return string 
     */
    public function getSex()
    {
        return $this->sex;
    }
    
    /**
     * Set firstName
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * Get firstName
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * Get lastName
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set birthDay
     * @param \DateTime $birthDay
     * @return User
     */
    public function setBirthDay($birthDay)
    {
        $this->birthDay = $birthDay;

        return $this;
    }

    /**
     * Get birthDay
     * @return \DateTime 
     */
    public function getBirthDay()
    {
        return $this->birthDay;
    }

    /**
     * Set maritalStatus
     * @param string $maritalStatus
     * @return User
     */
    public function setMaritalStatus($maritalStatus)
    {
        $this->maritalStatus = $maritalStatus;
        return $this;
    }

    /**
     * Get maritalStatus
     * @return string 
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * Set country
     * @param CountryInterface $country
     * @return User
     */
    public function setCountry(CountryInterface $country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     * @return CountryInterface 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     * @param CityInterface $city
     * @return User
     */
    public function setCity(CityInterface $city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return CityInterface 
     */
    public function getCity()
    {
        return $this->city;
    }

}
