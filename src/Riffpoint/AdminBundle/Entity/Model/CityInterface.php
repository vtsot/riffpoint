<?php

namespace Riffpoint\AdminBundle\Entity\Model;

/**
 * CityInterface
 */
interface CityInterface
{
    
    /**
     * @return string
     */
    public function __toString();

    /**
     * Get id
     * @return integer 
     */
    public function getId();

    /**
     * Set name
     * @param string $name
     * @return City
     */
    public function setName($name);

    /**
     * Get name
     * @return string 
     */
    public function getName();

    /**
     * Set country
     * @param CountryInterface $country
     * @return City
     */
    public function setCountry(CountryInterface $country);

    /**
     * Get country
     * @return \Riffpoint\AdminBundle\Entity\Country 
     */
    public function getCountry();
    
}
