<?php

namespace Riffpoint\AdminBundle\Entity\Model;

/**
 * WomanInterface
 */
interface WomanInterface extends PersonInterface
{
    
    /**
     * Валидатор пол пользователя
     * @return boolean
     */
    public function isValidSex();
    
    /**
     * Set breast
     * @param string $breast
     * @return Woman
     */
    public function setBreast($breast);

    /**
     * Get breast
     * @return string 
     */
    public function getBreast();

    /**
     * Set waist
     * @param string $waist
     * @return Woman
     */
    public function setWaist($waist);

    /**
     * Get waist
     * @return string 
     */
    public function getWaist();

    /**
     * Set hips
     * @param string $hips
     * @return Woman
     */
    public function setHips($hips);

    /**
     * Get hips
     * @return string 
     */
    public function getHips();
    
}
