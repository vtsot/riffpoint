<?php

namespace Riffpoint\AdminBundle\Entity\Model;

/**
 * MenInterface
 */
interface ManInterface extends PersonInterface
{
    
    /**
     * Валидатор пол пользователя
     * @return boolean
     */
    public function isValidSex();
    
    /**
     * Set bodyType
     * @param string $bodyType
     * @return Men
     */
    public function setBodyType($bodyType);
    
    /**
     * Get bodyType
     * @return string 
     */
    public function getBodyType();
    
}
