<?php

namespace Riffpoint\AdminBundle\Entity\Model;

use FOS\UserBundle\Model as FOSModel;

/**
 * PersonInterface
 * интерфейс персонализации
 */
interface PersonInterface extends FOSModel\UserInterface, FOSModel\GroupableInterface
{
    
    /**
     * Проверяет, можно ли удалять объект 
     * @return boolean
     */
    public function isUserValidForDelete();
    
    /**
     * Get id
     * @return integer 
     */
    public function getId();

    /**
     * Set sex
     * @param string $sex
     * @return User
     */
    public function setSex($sex);

    /**
     * Get sex
     * @return string 
     */
    public function getSex();

    /**
     * Set firstName
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName);

    /**
     * Get firstName
     * @return string 
     */
    public function getFirstName();

    /**
     * Set lastName
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName);

    /**
     * Get lastName
     * @return string 
     */
    public function getLastName();

    /**
     * Set birthDay
     * @param \DateTime $birthDay
     * @return User
     */
    public function setBirthDay($birthDay);

    /**
     * Get birthDay
     * @return \DateTime 
     */
    public function getBirthDay();

    /**
     * Set maritalStatus
     * @param string $maritalStatus
     * @return User
     */
    public function setMaritalStatus($maritalStatus);

    /**
     * Get maritalStatus
     * @return string 
     */
    public function getMaritalStatus();

    /**
     * Set country
     * @param CountryInterface $country
     * @return User
     */
    public function setCountry(CountryInterface $country);

    /**
     * Get country
     * @return CountryInterface 
     */
    public function getCountry();

    /**
     * Set city
     * @param CityInterface $city
     * @return User
     */
    public function setCity(CityInterface $city);

    /**
     * Get city
     * @return CityInterface 
     */
    public function getCity();
        
}
    