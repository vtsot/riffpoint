<?php

namespace Riffpoint\AdminBundle\Entity\Model;

/**
 * CountryInterface
 */
interface CountryInterface
{

    /**
     * @return string
     */
    public function __toString();
    
    /**
     * Get id
     * @return integer 
     */
    public function getId();

    /**
     * Set name
     * @param string $name
     * @return Country
     */
    public function setName($name);

    /**
     * Get name
     * @return string 
     */
    public function getName();

}
