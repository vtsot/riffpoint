<?php

namespace Riffpoint\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table()
 * @ORM\Entity()
 * @UniqueEntity(fields={"name"})
 */
class Country implements Model\CountryInterface
{

    // подключить Trait админ-генератора
    // Проверяет, можно ли удалять объект
    use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;
    
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="3", max="255")
     * @Assert\NotBlank()
     */
    protected $name;
    
    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->getName();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }
}
